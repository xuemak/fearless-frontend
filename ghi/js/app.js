function createCard(name, description, pictureUrl) { //get the details for each conference, pass it into this function, get a string back, and add it to the innerHTML of the columns
    return `
        <div class="shadow mb-5 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text">${description}</p>
            </div>
        </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => { // add 'async' to use 'await' for Promise objects that is being returned because you want the value from the Promise

    const url = 'http://localhost:8000/api/conferences/' //making a fetch for conferences

    try { //define a block of code to be tested for errors while it is being executed
        //prints out a Response object with a status code
        const response = await fetch(url)

        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            //getting the JSON from the response
            const data = await response.json()

            for (let conference of data.conferences) {
                //making a fetch for conference details
                const detailUrl = `http://localhost:8000${conference.href}` //creates the URL for the details of the conference since there is a href value for the conference
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    const detail = await detailResponse.json()
                    const title = detail.conference.name
                    const description = detail.conference.description
                    const pictureUrl = detail.conference.location.picture_url
                    const html = createCard(title, description, pictureUrl)
                    const column = document.querySelector('.col') //accessing 'col' class in the html document
                    column.innerHTML += html //innerHTML is a JS DOM property that can be used to get or set the HTML contents of an HTML element.
                }
            }
        }
    } catch (e) {
        // Figure out what to do if an error is raised
    }

});

// //getting the first conference out of the Conferences data array
// const conference = data.conferences[0]
// //use a CSS query selector to get a reference to the HTML tag for the card-title
// const nameTag = document.querySelector('.card-title')
// //set the innerHTML property of the element to set the name of the conference to the content of the element
// nameTag.innerHTML = conference.name


//     const conferenceDetail = detail.conference.description
//     const nameTagDetail = document.querySelector('.card-text')
//     nameTagDetail.innerHTML = conferenceDetail
//     const imageTag = document.querySelector('.card-img-top') //selects the img tag using its CSS class
//     imageTag.src = detail.conference.location.picture_url //set the src property of the returned value to the value of the picture URL in the details.
//     // console.log(detail)
