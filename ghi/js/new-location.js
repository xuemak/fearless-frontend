window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        const selectTag = document.getElementById('state') // Get the select tag element by its id 'state'
        for (let state of data.states) { // For each state in the states property of the data
            const dropDown = document.createElement('option') // Create an 'option' element
            dropDown.value = state.abbreviation // Set the '.value' property of the option element to the state's abbreviation
            dropDown.innerHTML = state.name  // Set the '.innerHTML' property of the option element to the state's name
            selectTag.appendChild(dropDown) // Append the option element as a child of the select tag
        }
    }
    const formTag = document.getElementById('create-location-form') // adding a submit event to the form
    formTag.addEventListener('submit', async (event) => { // the submit event handler
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const locationUrl = 'http://localhost:8000/api/locations/' //send the data to the server
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newLocation = await response.json()
        }
    })
})
