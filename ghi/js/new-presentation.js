window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'
    const response = await fetch(url)
    // console.log(response)
    if (response.ok) {
        const data = await response.json() // converting to js object
        // console.log(data)
        const selectTag = document.getElementById('conference') // Get the select tag element by its id 'location'
        for (let conference of data.conferences) { // For each location in the locations property of the data
            const dropDown = document.createElement('option') // Create an 'option' element
            dropDown.value = conference.id // Set the '.value' property of the option element to the location's id
            dropDown.innerHTML = conference.name  // Set the '.innerHTML' property of the option element to the location's name
            selectTag.appendChild(dropDown) // Append the option element as a child of the select tag
        }
    }
    const formTag = document.getElementById('create-presentation-form') // adding a submit event to the form
    formTag.addEventListener('submit', async (event) => { // the submit event handler
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const locationUrl = 'http://localhost:8000/api/conferences/' //send the data to the server
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newConference = await response.json()
        }
    })
})
